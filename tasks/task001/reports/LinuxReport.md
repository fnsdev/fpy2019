# Знакомство с Linux

Этот отчет содержит итоги моего знакомства с Linux.

## Рассмотренные темы

* [Введение](#intro)
    * [Что такое Linux?](#what-is-linux)
    * [Почему важно уметь работать с Linux?](#why-is-it-important)
* [Выбор дистрибутива](#linux-distributions)
* [Команда sudo](#sudo)
* [Знакомство с устройством файловой системы](#file-system)
* [Знакомство с менеджерами пакетов](#package-managers)
* [Знакомство с командной строкой](#command-line)
* [Базовые команды для работы с файловой системой](#working-with-file-system)
* [Перенаправление вывода](#input-output-redirect)
* [Пользователи и группы](#users-and-groups)
* [Права доступа](#permissions)
* [Процессы](#proccesses)
* [Написание bash скриптов](#bash-scripts)
* [Итоги проделанной работы](#outro)

## Intro

В этой части я постараюсь описать, почему следует изучать и использовать Linux.

### What is Linux

Обратимся к википедии:

> Linux - это семейство Unix-подобных операционных систем на базе ядра Linux, включающих некоторый набор утилит и программ, и возможно, другие компоненты.

Из этого определения сразу же возникает ряд вопросов:

* Что такое Unix-подобная ОС?
* Что такое ядро Linux?

Постараемся разобраться с каждым из них.

Не следует путать понятие **UNIX** с понятием **UNIX-подобная ОС**.
**UNIX** - это семейство операционных систем (Mac OS X, GNU/Linux), а **UNIX-подобная ОС** - это система, образованная под влиянием **UNIX**.

Слово UNIX используется и как знак соответствия, и как торговая марка.
Таким образом, следует отметить, что **UNIX** - это более широкое понятие, которое является фундаментом для построения и сертификация всех **UNIX-подобных ОС**, а **Linux** - это лишь частный случай **UNIX-подобной ОС**.

Подробнее можно почитать [здесь.](https://ru.wikipedia.org/wiki/Unix-%D0%BF%D0%BE%D0%B4%D0%BE%D0%B1%D0%BD%D0%B0%D1%8F_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0)

**Ядро Linux** - это ядро операционной системы, составляющее основу операционных систем семейства Linux. Код ядра в основном написан на *C* с некоторыми расширениями *gcc* и на *assembler*. 

Ядро Linux распространяется как свободное программное обеспечение на условиях *GNU General Public License.*

Подробнее [здесь](https://ru.wikipedia.org/wiki/%D0%AF%D0%B4%D1%80%D0%BE_Linux)

Таким образом, с техническоей точки зрения, Linux - это ядро, а не операционная система. Linux вместе с программами из проекта GNU образуют ОС GNU/Linux. Однако и она не существует в чистом виде. Разработчики дистрибутивов дорабатывают на свой лад GNU/Linux, после чего получаются различные операционные системы-дистрибутивы (*Arch Linux*, *Fedora*, *Ubuntu* и так далее).

### Why is it important

Здесь я приведу некоторые аргументы, которые позволят понять важность умения работать с ОС семейства Linux.

* Гибкость и настраиваемость системы.
* Бесплатность
* Open Source
* Удобство работы через командую строку (важно, если нет доступа к GUI)

Дистрибутивы Linux уже давно используются как операционные системы для серверов и занимают значительную часть этого рынка.

Кроме того, Linux является ключевым компонентом комплекса серверного комплекта программного обеспечения LAMP (Linux, Apache, MySQL, Perl/PHP/Python), который приобрел большую популярность у web-разработчиков и стал одной из наиболее распространенных платформ для хостинга веб-сайтов.

Также дистрибутивы Linux используются в качестве ОС суперкомпьютеров и мейнфреймов.

## Linux Distributions

На сегодняшний день существует огромное количество различных дистрибутивов Linux и их вариаций. Для знакомства с новой операционной системой я решил установить один из наиболее дружелюбных для новых пользователей дистрибутивов - **Ubuntu.**

Кратко перечислю ее основные особенности:

* Простота установки
* Дружелюбный графический интерфейс на основе GNOME
* Большое сообщество пользователей
* Хорошая поддержка
* Предустановленные базовые программы, такие как архиваторы, браузер и другие.
* Широкое использование утилиты *sudo* для выполнения администраторских задач без необходимости запускать потенциально опасную сессию суперпользователя

Все дальнейшие действия будут рассмотрены с точки зрения пользователя Ubuntu.

## Sudo

Утилита **sudo** позволяет первому пользователю, созданному при установке системы, выполнять команды с правами администратора. При этом не требуется входить в систему как пользователь *root*, что позволяет обезопасить себя от бесконтрольного манипулирования важными файлами.

Пример использования команды **sudo:**

`sudo apt isntall python3-pip`

## File System

Структура файловой системы Ubuntu отличается от Windows. 
Установка Ubuntu требует как минимум одного раздела:

* Корневой раздел, обозначаемый '/'

В версии Ubuntu 18.04 отдельный swap-раздел при установке сисетемы не является обязательным для создания, вместо него используется так называемый *swap файл.* 

Кроме того, по желанию можно создать второй раздел - *Home,* на котором будут храниться настройки приложений и файлы пользователя.

Ubuntu как правило устанавливается на раздел с файловой системой *ext4*. При этом Ubuntu может записывать и считывать из разделов *FAT16*, *FAT32*, *VFAT*, *NTFS*.

Linux, в отличие от DOS и Windows, не назначает буквы каждому диску и разделу. Вместо этого Linux работает по принципу иерархического дерева каталогов, где корневой каталог (/) является основной точкой монтирования, в которую по умолчанию входят все остальные. Все используемые разделы дисков монтируются в подкаталоги корня, а не как отдельные устройства.

Рассмотрим описание основных директорий:

| Директория | Описание |
| :--- | :--- |
| / | Корневая директория, содержащая всю файловую иерархию |
| /bin/ | Основные системные утилиты |
| /boot/ | Загрузочные файлы |
| /dev/ | Основные файлы устройств системы |
| /etc/ | Общесистемные конфигурационные файлы |
| /home/ | Содержит домашние директории пользователей |
| /lib/ | Основные библиотеки, необходимые для работы программ из /bin/ и /sbin/ |
| /opt/ | Дополнительное ПО |
| /sbin/ | Основные системные программы для администрирования |
| /root/ | Домашняя директория пользователя root |
| /tmp/ | Временные файлы |
| /usr/ | Вторичная иерархия для данных пользователя |
| /var/ | Изменяемые файлы, такие как файлы регистрации, временные почтовые файлы, кэш приложений |

## Package Managers

Программы для Ubuntu поставляются в виде так называемых deb-пакетов. Deb-пакет - это обычный архив, содержащий файлы устанавливаемого приложения и различную вспомогательную информацию. Программы и обновления в Ubuntu устанвливаются преимущественно из *репозиториев.*

Репозитории – это специальные сервера-хранилища deb-пакетов. Подробнее про подключение дополнительных репозиториев можно прочитать [здесь](https://help.ubuntu.ru/wiki/%D1%80%D0%B5%D0%BF%D0%BE%D0%B7%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%B9)

Важным отличием от Windows является то, что deb-пакеты оставляют вопрос предоставления необходимых библиотек на совесть системы. Это делается с помощью указания зависимостей. Если система не может разрешить зависимости, то пакет не будет установлен. 

Ubuntu разрешает все зависимости автоматически и скачивает недостающие пакеты из интернета. Если интернет отсутствует, потребуется ручное разрешение зависимостей.

Я не буду рассматривать установку приложений с помощью Центра приложений Ubuntu, поскольку это довольно тривиальная задача (ознакомиться можно [здесь](https://help.ubuntu.ru/wiki/%D1%86%D0%B5%D0%BD%D1%82%D1%80_%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B9_ubuntu)), а вместо этого сконцетрирую внимание на пакетном менеджере **APT**.

**Advanced Packaging Tool** - набор утилит для управления программными пакетами в операционных системах, основанных на Debian. APT предоставляет дружественную надстройку над более низкоуровневой утилитой **DPKG** и позволяет делать следующее:

* Устанавливать, удалять и обновлять пакеты
* Решать зависимости
* Искать пакеты по заданным критериям
* Просматривать подробную информацию о пакете

### Использование APT

Начиная с версии Ubuntu 16.04 доступна новая утилита apt, которая содержит в себе наиболее часто используемые команды из apt-get и apt-cache, однако ими по-прежнему можно пользоваться.

Рассмотрим базовые команды утилиты apt (большинство потребуется запустить с правами суперпользователя):

| Команда | Описание |
| :--- | :--- |
| `update` | Обновить информацию о пакетах, содержащихся в репозиториях |
| `install <package>` | Установить пакет *package*. Скачивание, настройка и установка происходят автоматически, если для настройки пакета не нужны дополнительные сведения |
| `upgrade` | Обновление пакетов, для которых в репозитории доступны новые версии |
| `remove <package>` | Удаление пакета *package* из системы |
| `purge <package>` | Удаление пакета *package* и очистка системы от его конфигурационных файлов. |
| `source <package>` | Получение исходных текстов пакета *package* |
| `search` | Поиск пакета по части названия или описания |
| `show` | Информация о пакете |
| `depends` | Зависимости пакета |
| `rdepends` | Обратные зависимости пакета |

## Command Line

С точки зрения рядового пользователя, привыкшего работать с оконными приложениями, *Command Line Interface (CLI)* - устаревшая технология. Однако тяжело переоценить важность CLI для разработчиков, которые зачастую не имеют доступа к графическому интерфейсу на удаленной машине. 

При должных навыках, любую работу с Linux можно осуществить с помощью командной строки. Во многих дистрибутивах Linux можно перейти из графического в текстовый режим работы, в котором полностью исчезают элементы графического интерфейса и курсор мыши.

Отметим несколько из преимуществ интерфейса командной строки:

* Более быстрый доступ к некоторым возможностям системы
* Менее требователен к ресурсам 
* Зачастую GUI не нужен, например, на серверах
* Позволяет автоматизировать работу системы с помощью скриптов

Базовым способом взаимодействия с командной строкой на Ubuntu является командная оболочка **Bash**.

**Bash** на Ubuntu запускается в эмуляторе текстового режима - так называемом *терминале.* После запуска он работает в интерактивном режиме, ожидая ввода от пользователя. Введенная строка интерпретируется как команда, которую необходимо выполнить. Затем терминал опять ожидает ввода новой строки.

Существую самые различные команды - от запуска программ и работы с файловой системой до выполнение скриптов, написанных на языке самого Bash, который будет рассмотрен далее.

Здесь мне хотелось бы только отметить одну из наиболее важных bash команд - **man**. Она позволяет получить руководство по использованию других bash команд, а также все доступные опции запуска этих команд, что невероятно полезно при освоении работы с терминалом. Пример использования:

```
    $ man ls
```

Отличной книгой для изучения bash является [The Linux Command Line.](http://linuxcommand.org/tlcl.php) 


## Working with file system

Не вдаваясь в подробности, кратко рассмотрим основые команды для работы с файловой системой Linux. Подробное их описание и параметры можно изучить с помощью команды *man* или в книге, указанной выше.

| Команда | Описание |
| :--- | :--- |
| `pwd` | Вывести текущий абсолютный путь |
| `ls </path/to/directory>` | Отобразить все файлы и директории |
| `cd </path/to/directory>` | Изменить текущую директорию |
| `touch <file_name>` | Создать файл *file_name* |
| `cat <file_name>` | Отобразить содержимое файла |
| `cp <file1> <file2>` | Копировать *file1* по пути *file2* |
| `mv <file1> <file2>` | Переместить *file1* по пути *file2* |
| `rm <file_name>` | Удалить файл *file_name* |
| `mkdir <direcoty>` | Создать директорию с именем *directory* |
| `locate <file>` | Возвращает все пути с вхождением *file* в них|
| `grep <"pattern"> <file_name>` | Поиск по содержимому файла |

Подробнее с работой этих команд можно ознакомиться с помощью команды `man` или в указанной выше книге.

## Input Output Redirect

В системе по умолчанию всегда открыты 3 'файла': *stdin* (поток ввода), *stdout* (поток вывода) и *stderr* (поток ошибок). Очень удобным функционалом является возможность перенаправить эти потококи. Сделать это в bash можно следующим образом:

* < file - Использовать файл как источник данных для стандартного потока ввода
* \> file - Направить стандартный поток вывода в файл. Если файл не существует, он будет создан, если существует — перезаписан
* 2> file - Направить стандартный поток ошибок в файл. Если файл не существует, он будет создан, если существует — перезаписан
* \>>file - Направить стандартный поток вывода в файл. Если файл не существует, он будет создан, если существует — данные будут дописаны к нему в конец
* 2>>file - Направить стандартный поток ошибок в файл. Если файл не существует, он будет создан, если существует — данные будут дописаны к нему в конец
* &>file или >&file - Направить стандартный поток вывода и стандартный поток ошибок в файл

Кроме того существует возможность перенаправить поток из одной программы в другую. Стандартный вывод данных после выполнения одной команды перенаправляется в другую через канал. На дисплей терминала при этом буду отображены только данные возвращаемые другой командой. Это можно сделать с помощью символа `|`.

`$ ls | less`
`$ cat file | grep 'text'`

## Users and groups

Linux является многопользовательской системой. Механизм пользователей и групп пользователей используется в Linux для управления доступом к файлам. 

Имена пользователей могут быть любыми, однако кажому аккаунту должно соответствовать уникальное имя. Есть несколько зарезервированных имен, которые нельзя использовать, например `root`, `adm`.

### Управление пользователями

#### Добавление пользователя

Добавление нового пользователя осуществляется с помощью команды `useradd`:

`$ sudo useradd vasya`

Команда имеет ряд параметров, которые позволяют изменить настройки создаваемого пользователя. Вот некоторые из них:

| Ключ команды | Описание команды |
| :--- | :---  |
| -g | Первичная группа пользователя |
| -G | Вторичные группы пользователя |
| -e | Дата, после которой пользователь будет отключен |
| -d | Название домашнего каталога |
| -c | Любой текстовый комментарий  |

#### Изменение настроек пользователя

Изменение настроек пользователя осуществляется с помощью утилиты `usermod`. Можно использовать все те же параметры, кто и у команды `useradd`, например:

`$ sudo usermod -c "New comment" vasya`

#### Изменение пароля

Изменить пароль можно при помощи утилиты: `passwd`:

`$ sudo passwd vasya`

Некоторые параметры утилиты:

| Ключ команды | Описание команды |
| :--- | :---  |
| -d| Удалить пароль пользователю. Он сможет входить в систему без пароля |
| -n | Минимальное кол-во дней между сменами пароля |
| -e | Сделать пароль устаревшим. При следующем входе пользователю придется изменить пароль |
| -i | Заблокировать пользователя через указанное кол-во дней после устаревания пароля  |

#### Получение информации о пользователях:

Здесь я приведу несколько команд, которые позволяют получить некоторую информацию о пользователях.

* `w` - вывод информации о всех вошедших в систему пользователях (`who` - чуть меньше информации)
* `whoami` - вывод имени вашего пользователя
* `users` - вывод имен пользователей, работающих в системе
* `groups <user_name>` - вывод списка групп, в которых состоит пользователь

#### Удаление пользователя

Удалить пользователя можно с помощью команды `userdel`:

`$ sudo userdel vasya`

### Управление группами

#### Создание группы

Чтобы создать группу, нужно воспользоваться утилитой `groupadd`:

`$ sudo groupadd new_group`

Основные ключи:

| Ключ команды | Описание команды |
| :--- | :---  |
| -g| Установить собственный GID |
| -p | Пароль группы |
| -r | Создать системную группу |

#### Изменение группы

Сменить название группы, ее GID или пароль можно с помощью утилиты `groupmod`:

`$ sudo groupmod -n old_group new_group` - изменить название группы `new_group` на `old_group`.

#### Удаление группы

`$ sudo groupdel old_group`

#### Добавление пользователя в группу

С помощью утилиты `usermod` (см. выше).

## Permissions

В силу того, что Linux является многопользовательской системой, у любого файла должны быть специальные атрибуты - права на доступ. 

Существует 3 категории пользователей, которым могут быть предоставлены права на файл:

* Владелец (**u -user**) файла - пользователь, имя которого записано в атрибуты файла как имя владельца
* Группа (**g -group**) - группа, к которой принадлежит владелец файла
* Остальные (**o -other**) - все остальные пользователи

### Операции чтения, записи и выполнения

Чтение, запись и выполнение - это операции, которые можно произвести с существующим файлом. У каждой категории пользователей должны быть свои права на каждую из этих операций.

|Название|Описание|
|:---|:---|
|r -read|Файл доступен для просмотра|
|w -write|Файл доступен для записи|
|x -execution|Файл доступен для исполнения как программа|

Таким образом, в атрибутах каждого файла должно быть 9 записей о правах, которые указывают на то, кто и как может взаимодействовать с данным файлом.

Варианты записи прав доступа:

* Буквенная
* Численная
    * В двоичной системе
    * В восьмеричной системе

### Буквенная запись

Первые три буквы - права владельца, вторые три буквы - права группы, последние три буквы - права всех остальных.
Выглядит это следующим образом:

`rwxrwxrwx`, `rw-r--r--`, `r--r-----` и так далее.

### Численная запись
#### В двоичной системе

Аналогично буквенной записи, но на месте символов `r, w, x` записываются нули либо единицы. Единица - когда право есть, а ноль - когда отсутствует. Примеры:
`111111111`, `110100100`, `100100000` и так далее.

#### В восьмеричной системе

Для сокращения записи, используют восьмеричную систему. Для наглядности рассмотрим таблицу соответствия:

|Буквенное|Двоичное|Восьмеричное|Смысл|
|- - -|---|---|---|
|---|000|0|Все запрещено|
|--x|001|1|Только исполнение|
|-w-|010|2|Только запись|
|-wx|011|3|Запись и исполнение|
|r--|100|4|Только чтение|
|r-x|101|5|Чтение и исполнение|
|rw-|110|6|Чтение и запись|
|rwx|111|7|Все разрешено|


### Права доступа к каталогам

Для каталогов смысл атрибутов `-r, -w, -x` другой.

* Право на чтение означает возможность узнать список файлов в каталоге.
* Право на запись означает возможность удалять, переименовывать и создавать новые файлы в каталоге.
* Право на исполнение означает возможность заходить в каталог, просматривать и изменять содержимое файлов, если на них есть разрешение, узнавать свойства файлов.

### Изменение прав доступа

Для изменения прав доступа к файлу или каталогу используется команда **chmod.**

Изменять права можно либо с помощью буквенной записи, либо с помощью записи в восьмеричной системе.

Примеры исполнения:

`$ chmod 744 file.txt`
`$ chmod ugo=rw- file.txt`

Подробнее про **chmod** для работы с каталогами можно почитать [здесь.](https://younglinux.info/chmod)

## Processes

Для управления процессами в Linux существут ряд полезных команд. Я хотел бы рассмотреть лишь некоторые из них. Подробнее можно прочитать, например, [здесь](http://www.linuxcenter.ru/lib/books/kostromin/gl_08_04.phtml)

### Просмотр запущенных процессов

Для просмотра запущенных в текущей сессии терминала процессов используется команда `ps`

`$ ps`
`$ ps -ax` - список процессов, запущенных в системе.

Команда обладает огромным количеством параметров, прочитать про которые можно по ссылке выше.

В отличие от команды `ps`, которая позволяет получить список процессов на момент вызова, команда `top` отображает состояние процессов и их активность в 'реальном времени' в окне терминала.

Здесь также важно отметить, что каждый процесс обладает уникальным идентификатором - *PID*.

`$ top`

### Приоритеты процессов

В Linux, как и во всех современных операционных системах, одновременно может выполняться несколько процессов. Это достигается путем разделения процессорных и других ресурсов между процессами. 

Когда количество имеющихся в нашем распоряжении процессоров ограничено, необходимо решить, как распределить имеющиеся ресурсы между несколькими конкурирующими процессами. Обычно эта задача решается так: выбирается какой-то один процесс, который выполняется на протяжении короткого отрезка времени либо до тех пор, пока он не переходит в состояние ожидания какого-либо события, например, завершения операции ввода/вывода.

Чтобы важные процессы всегда имели необходимые им процессорные ресурсы, выбор производится на базе распределения машинного времени. 

Приоритет для каждого процесса определяется значением *nice*, которое лежит в интервале от -20 (наивысший приоритет) до +20 (наименьший приоритет) устанавливается в момент порождения этого процесса.

Изменить приоритет процесса при его запуске позволяет команда `nice`, а изменить приоритет уже выполняющегося процесса - команда `renice`.

### Сигналы

Сигналы - это средство, с помощью которого процессам можно передать сообщения о некоторых событиях в системе. С помощью сигналов можно, например, приостанавливать, продолжать выполнение, завершать работу процесса. Просмотреть список всех доступных сигналов можно с помощью команды `kill -l`. Каждый сигнал имеет свой номер.

Когда процесс получает сигнал, то возможен один из двух вариантов развития событий. Если для данного сигнала определена подпрограмма обработки, то вызывается эта подпрограмма. В противном случае ядро выполняет от имени процесса действие, определенное по умолчанию для данного сигнала. Вызов подпрограммы обработки называется перехватом сигнала. Когда завершается выполнение подпрограммы обработки, процесс возобновляется с той точки, где был получен сигнал.

Несколько часто встречающихся сигналов:

| Номер | Название | Описание |
| :--- | :--- | :---  |
| 1 | HUP | Отбой |
| 2 | INT | Прекращение выполнения (простые команды) или прекращение активного процесса (интерактивные программы) |
| 3 | QUIT | Сильнее сигнала INT |
| 9 | KILL| Всегда прекращает выполнения процесса |
| 15 | TERM | Требование закончить процесс |
| 19 | STOP | Приостановка выполнения процесса |
| 18 | CONT | Продолжение выполнения приостановленного процесса |

Чтобы отправить сигнал процессу, необходимо воспользоваться командой `kill` в следующем формате:
`$ sudo kill [signal_number] <PID>`

При этом, по умолчанию посылается сигнал номер 15.

### Перевод процесса в фоновый режим

По умолчанию при запуске процессов через терминал, они привязываются к этому терминалу, воспринимают ввод из него и осуществляют вывод на него. Но можно запустить процесс в фоновом режиме без связи с терминалом.

Чтобы запустить процесс в фоновом режиме через терминал, необходимо в конце командной строки при его запуске добавить символ `&`:

`$ discord &` - запуск *discord* без привязки к терминалу

## Bash Scripts

Bash-скрипты - это сценарии командной строки, написанные для оболочки bash. Такие файлы имеют расширение `.sh`.

Сценарии командной строки - это наборы команд, которые можно вводить в терминал с клавиатуры вручную, собранные в файлы и объединенные общей целью. При этом результаты работы команд могут представлять собой либо самостоятельную ценность, либо служить входными данными для других команд. Сценарии - это мощный инструмент для автоматизации часто выполняемых действий.

Обязательной частью bash-скрипта является первая строка, указывающая путь к интерпретатору bash.

`#!bin/bash`

Команды отделяются знаком перевода строки, комментарии выделяют знаком решетки.

```bash
    #!bin/bash
    # Comment
    pwd
    ls
    cd ~/
```

Для того, чтобы сценарий можно было исполнить, ему необходимо дать права на исполнение. Сделать это можно следующим образом:

`$ chmod +x script.sh`
`$ ./script.sh` 


Подробнее ознакомиться с синтаксисом можно в книге [The Linux Command Line](http://linuxcommand.org/tlcl.php) или в [цикле статей.](https://habr.com/ru/company/ruvds/blog/325522/) 

Для удобства, мною был написан скрипт, который активирует виртуальное окружение Python, а затем запускает тестовый сервер Django. Для этого ему передается путь к директории, в которой находится файл `manage.py`. Выглядит скрипт следующим образом:

```bash
    #!/bin/bash

    path=$1
    if [ -d $path ] & [ -e "$path/manage.py" ]
    then
        cd $path
        cd ..
        source venv/bin/activate
        cd $path
        python manage.py runserver
    else
        echo 'Wrong directory'
    fi
``` 

*Подразумевается, что виртуальное окружение было создано на один уровень иерархии выше, чем расположение файла `manage.py`*

## Outro

По итогам ознакомления с операционной системой Ubuntu хотелось бы отметить ряд важных моментов, которые были мною усвоены в процессе:

* Linux - важный инструмент для разработчика
* Но и для повседневого использования вполне подходит
* Файловая система отличается от той, к которой я привык в Windows
* Но разобраться в отличиях не сильно сложно
* Умение работать с командой строкой - ключевой навык при использовании Linux
* Bash-скрипты - мощный инструмент, который я продолжу осваивать в дальнейшем


